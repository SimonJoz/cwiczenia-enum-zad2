package com.jdw;

import java.util.Scanner;

public class Pizzeria {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = 1;
        for (Pizza value : Pizza.values()) {
            System.out.println(i + "." + value);
            i++;
        }
        System.out.println("Witch one you'd like to buy ?");
        String choice = scanner.nextLine();
        String goodbye = "\nThank you and Bon Appetit :)";
        Pizza pizza = Pizza.valueOf(choice.toUpperCase());
        System.out.println("You choose: " + pizza + goodbye);

//        switch (choice) {
//            case "1":
//                System.out.println("You choose: " + Pizza.MARGHERITA + goodbye);
//                break;
//            case "2":
//                System.out.println("You choose: " + Pizza.CAPRICIOSA + goodbye);
//                break;
//
//            case "3":
//                System.out.println("You choose: " + Pizza.PROSCIUTTO + goodbye);
//
//                break;
//            default:
//                System.out.println("Sorry but we haven't got that pizza in our menu..");
//        }
    }
}

