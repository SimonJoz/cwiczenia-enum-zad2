package com.jdw;

public enum Pizza {
    MARGHERITA(true, false, false, true),
    CAPRICIOSA(true, true, false, true),
    PROSCIUTTO(true, true, true, false);

    private final boolean sos;
    private final boolean chees;
    private final boolean ham;
    private final boolean mashrums;

    Pizza(boolean sos, boolean chees, boolean ham, boolean mashrums) {
        this.sos = sos;
        this.chees = chees;
        this.ham = ham;
        this.mashrums = mashrums;
    }

    @Override
    public String toString() {
        String pizza = name() + " (";
        if (sos) pizza += "sos";
        if (chees) pizza += ", chees";
        if (ham) pizza += ", ham";
        if (mashrums) pizza += ", mashrums";
        pizza += ")";
        return pizza;
    }
}
